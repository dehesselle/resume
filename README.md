# resume

Click [👉 here](https://gitlab.com/dehesselle/resume/-/jobs/artifacts/master/file/resume.html?job=build) to read my resume.

💁 _The link above points to a generated HTML page (see [`.gitlab-ci.yml`](.gitlab-ci.yml)) and will trigger the following security warning:_

> You are being redirected away from GitLab

_Sorry for the inconvenience!_

## credits

Built with [JSON Resume](https://jsonresume.org) which is licensed under MIT.

## notes

- company names have changed over time due to reorganizations
- old URLs no longer exist, using recent ones
